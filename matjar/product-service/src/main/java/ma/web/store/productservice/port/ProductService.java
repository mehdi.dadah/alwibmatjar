package ma.web.store.productservice.port;

import ma.web.store.productservice.adapter.rest.dto.ProductRequest;
import ma.web.store.productservice.adapter.rest.dto.ProductResponse;
import ma.web.store.productservice.domain.model.Product;

public interface ProductService {

    Product getById(Long productId);

    ProductResponse create(ProductRequest productRequest);

    ProductResponse update(Long productId, ProductRequest productRequest);

    ProductResponse getProductByBarCodeOrProductName(Long productBarCode, String productName);

    void updateStock(Long productBarCode, Integer quantity);

    void deleteById(Long productId);
}
