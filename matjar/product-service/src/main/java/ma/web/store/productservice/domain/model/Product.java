package ma.web.store.productservice.domain.model;

import io.swagger.annotations.ApiModel;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@ApiModel(value = "Product")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id", updatable = false)
    @Setter(AccessLevel.NONE)
    private Long productId;

    @Column(name = "product_bar_code", nullable = false, unique = true)
    private Long productBarCode;

    @Column(name = "product_name", nullable = false, unique = true, length = 130)
    private String productName;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "product_description")
    private String productDescription;

    @Column(name = "product_stock", nullable = false)
    private Integer productStock;

    @Column(name = "product_price", nullable = false)
    private Double productPrice;

    @CreationTimestamp
    @Column(name = "created_date", insertable = false, updatable = false)
    private LocalDate createDate;

    @UpdateTimestamp
    @Column(name = "last_modified_date", insertable = false)
    private LocalDateTime lastModifiedDate;
}
