package ma.web.store.productservice.adapter.rest.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@ApiModel(value = "ErrorDto")
@Getter
@SuperBuilder
public class ErrorDto {

  protected String title;
  protected int status;
  protected String detail;
  protected String exceptionClassName;
  protected LocalDateTime timestamp;

}
