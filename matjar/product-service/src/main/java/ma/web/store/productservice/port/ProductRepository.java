package ma.web.store.productservice.port;

import ma.web.store.productservice.domain.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    Optional<Product> findByProductBarCodeOrProductName(Long productBarCode, String productName);

    @Modifying(clearAutomatically = true)
    @Query("UPDATE Product AS p SET p.productStock = p.productStock + (:stock) WHERE p.productBarCode = :productBarCode")
    int updateStockByBarCode(@Param("stock") Integer stock, @Param("productBarCode") Long productBarCode);

}
