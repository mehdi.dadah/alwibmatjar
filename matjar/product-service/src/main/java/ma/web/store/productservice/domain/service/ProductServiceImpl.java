package ma.web.store.productservice.domain.service;

import ma.web.store.productservice.adapter.rest.dto.ProductRequest;
import ma.web.store.productservice.adapter.rest.dto.ProductResponse;
import ma.web.store.productservice.adapter.rest.exception.ResourceNotFoundException;
import ma.web.store.productservice.adapter.rest.mapper.ProductMapper;
import ma.web.store.productservice.domain.model.Product;
import ma.web.store.productservice.port.ProductRepository;
import ma.web.store.productservice.port.ProductService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    private static final String PRODUCT_NOT_FOUND = "Product Not Found";

    public ProductServiceImpl(ProductRepository productRepository,
                              ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Override
    public Product getById(Long productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException(PRODUCT_NOT_FOUND));
    }

    @Transactional
    @Override
    public ProductResponse create(ProductRequest productRequest) {
        Product product = productMapper.productRequestToProduct(productRequest);
        Product savedProduct = productRepository.save(product);
        return productMapper.productToProductResponse(savedProduct);
    }

    @Transactional
    @Override
    public ProductResponse update(Long productId, ProductRequest productRequest) {
        Product product = getById(productId);
        product.setProductBarCode(productRequest.getProductBarCode());
        product.setProductName(productRequest.getProductName());
        product.setProductDescription(productRequest.getProductDescription());
        product.setProductStock(productRequest.getProductStock());
        product.setProductPrice(productRequest.getProductPrice());

        Product savedProduct = productRepository.save(product);
        return productMapper.productToProductResponse(savedProduct);
    }

    @Transactional(readOnly = true)
    @Override
    public ProductResponse getProductByBarCodeOrProductName(Long productBarCode, String productName) {
        Optional<Product> product = productRepository.findByProductBarCodeOrProductName(productBarCode, productName);
        return productMapper.productToProductResponse(product.orElseThrow(() -> new ResourceNotFoundException(PRODUCT_NOT_FOUND)));
    }

    @Transactional
    @Override
    public void updateStock(Long productBarCode, Integer quantity) {
        if (productRepository.updateStockByBarCode(quantity, productBarCode) < 1) {
            throw new ResourceNotFoundException(PRODUCT_NOT_FOUND);
        }
    }

    @Transactional
    @Override
    public void deleteById(Long productId) {
        productRepository.delete(getById(productId));
    }
}
