package ma.web.store.productservice.adapter.rest.mapper;

import ma.web.store.productservice.adapter.rest.dto.ProductRequest;
import ma.web.store.productservice.adapter.rest.dto.ProductResponse;
import ma.web.store.productservice.domain.model.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

    public Product productRequestToProduct(ProductRequest productRequest) {
        return Product.builder()
                .productBarCode(productRequest.getProductBarCode())
                .productName(productRequest.getProductName())
                .productDescription(productRequest.getProductDescription())
                .productStock(productRequest.getProductStock())
                .productPrice(productRequest.getProductPrice())
                .build();

    }

    public ProductResponse productToProductResponse(Product product) {
        return ProductResponse.builder()
                .productId(product.getProductId())
                .productBarCode(product.getProductBarCode())
                .productName(product.getProductName())
                .productDescription(product.getProductDescription())
                .productStock(product.getProductStock())
                .productPrice(product.getProductPrice())
                .createDate(product.getCreateDate())
                .lastModifiedDate(product.getLastModifiedDate())
                .build();
    }
}
