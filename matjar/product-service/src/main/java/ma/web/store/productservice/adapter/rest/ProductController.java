package ma.web.store.productservice.adapter.rest;

import io.micrometer.core.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import ma.web.store.productservice.adapter.rest.dto.ProductRequest;
import ma.web.store.productservice.adapter.rest.dto.ProductResponse;
import ma.web.store.productservice.port.ProductService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

@Api(tags = "product")
@RestController
@RequestMapping("/api/products")
public class ProductController {

    private static final MediaType JSON = MediaType.APPLICATION_JSON;

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @Timed
    @Operation(summary = "Create product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Create the Product",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ProductResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)})
    @PostMapping
    public ResponseEntity<ProductResponse> create(@Valid @RequestBody ProductRequest productRequest){
        return ResponseEntity
                .status(CREATED)
                .contentType(JSON)
                .body(productService.create(productRequest));
    }

    @Timed
    @Operation(summary = "Update product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update the Product",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ProductResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)})
    @PutMapping("/{productId}")
    public ResponseEntity<ProductResponse> update(@PathVariable Long productId, @Valid @RequestBody ProductRequest productRequest){
        return ResponseEntity
                .ok()
                .contentType(JSON)
                .body(productService.update(productId, productRequest));
    }

    @Timed
    @Operation(summary = "Update stock")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update the Stock Product",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ProductResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)})
    @PutMapping("/{productBarCode}/stock")
    public ResponseEntity<Void> updateStock(@PathVariable Long productBarCode, @RequestParam() Integer quantity){
        productService.updateStock(productBarCode, quantity);
        return ResponseEntity
                .noContent()
                .build();
    }

    @Timed
    @Operation(summary = "Delete product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Delete Product By Id",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ProductResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)})
    @DeleteMapping("/{productId}")
    public ResponseEntity<Void> deleteById(@PathVariable Long productId){
        productService.deleteById(productId);
        return ResponseEntity
                .noContent()
                .build();
    }

    @Timed
    @Operation(summary = "Get product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get Product By Bare Code or product name",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ProductResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)})
    @GetMapping("/responses")
    public ResponseEntity<ProductResponse> getProductByBarCodeOrProductName(@RequestParam(defaultValue = "0", required = false) Long productBarCode,
                                                              @RequestParam(defaultValue = " ", required = false) String productName){
        return ResponseEntity
                .ok()
                .contentType(JSON)
                .body(productService.getProductByBarCodeOrProductName(productBarCode, productName));
    }


}
