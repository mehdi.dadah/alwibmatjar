package ma.web.store.productservice.adapter.rest.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@ApiModel(value = "ProductResponse")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductResponse {

    private Long productId;
    private Long productBarCode;
    private String productName;
    private String productDescription;
    private Integer productStock;
    private Double productPrice;
    private LocalDate createDate;
    private LocalDateTime lastModifiedDate;
}
